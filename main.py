# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


import random
import statistics
import copy
import tkinter
import cv2
import numpy as np
import pyautogui

# Define the game board size and the number of different gem types
BOARD_SIZE = 8
NUM_GEM_TYPES = 7

red_mana = 0
green_mana = 0
blue_mana = 0
yellow_mana = 0
brown_mana = 0
purple_mana = 0
extra_turn = False
blue_storm = False
green_storm = False
red_storm = False
yellow_storm = False
purple_storm = False
brown_storm = False
bone_storm = False
blue_banner = 0
green_banner = 0
red_banner = 0
yellow_banner = 0
purple_banner = 0
brown_banner = 0
blue_mastery_chance = 0
green_mastery_chance = 0
red_mastery_chance = 0
yellow_mastery_chance = 0
purple_mastery_chance = 0
brown_mastery_chance = 0
window = tkinter.Tk()
window.title("Gems of War Extra Turn Finder")
gemframe = tkinter.Frame(window)
gemframe.pack(side=tkinter.LEFT)
buttonframe = tkinter.Frame(window)
buttonframe.pack(side=tkinter.RIGHT)
infoframe = tkinter.Frame(window)
infoframe.hidden=1
infoTextVar = tkinter.StringVar()
teamselectframe = tkinter.Frame(window)
teamselectframe.hidden = 1
verboseoutput= False

#teams
divineishbaala = tkinter.BooleanVar()
draxxius = tkinter.BooleanVar()
taipan = tkinter.BooleanVar()
eldritchminion = tkinter.BooleanVar()
baneofmercy = tkinter.BooleanVar()
mercy = tkinter.BooleanVar()
veneratus = tkinter.BooleanVar()
malcandessa = tkinter.BooleanVar()
moonsinger = tkinter.BooleanVar()
astralspirit = tkinter.BooleanVar()
apophisis = tkinter.BooleanVar()
trknala = tkinter.BooleanVar()
gimletstormbrew = tkinter.BooleanVar()
graveseer = tkinter.BooleanVar()
lamashtu = tkinter.BooleanVar()
scurvyseadog = tkinter.BooleanVar()
inari = tkinter.BooleanVar()
childofsummer = tkinter.BooleanVar()
firstmateaxelubber = tkinter.BooleanVar()
moonrabbit = tkinter.BooleanVar()
spiderthrone = tkinter.BooleanVar()
fistofzorn = tkinter.BooleanVar()
drownedsailor = tkinter.BooleanVar()
daughterofice = tkinter.BooleanVar()
heraldofdamnation = tkinter.BooleanVar()
spiritfox = tkinter.BooleanVar()
yaoguai = tkinter.BooleanVar()
doomedaxe = tkinter.BooleanVar()
doomedscythe = tkinter.BooleanVar()
doomedglaive = tkinter.BooleanVar()
doomedclub = tkinter.BooleanVar()
doomedcrossbow = tkinter.BooleanVar()
doomedblade = tkinter.BooleanVar()
alderfather = tkinter.BooleanVar()
nimue = tkinter.BooleanVar()
forestguardian = tkinter.BooleanVar()
qilin = tkinter.BooleanVar()
sekhma = tkinter.BooleanVar()
beastmastertorbern = tkinter.BooleanVar()
infernalking = tkinter.BooleanVar()
sirquentinhadley = tkinter.BooleanVar()
wrath = tkinter.BooleanVar()
glaycion = tkinter.BooleanVar()

# Define the gem colors and their symbols
GEM_COLORS = {
    'red': 'R',
    'green': 'G',
    'blue': 'U',
    'yellow': 'Y',
    'brown': 'B',
    'purple': 'P',
    'skull': 'S',
    'blank': ' '
}
GEM_COLORS_LIST = list(GEM_COLORS.items())


# Define a function to create a new game board
def create_board():
    board = [[random.randint(1, NUM_GEM_TYPES) for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]
    return board


def create_mana_board():
    mana_board = [[0 for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]
    return mana_board


# Define a function to display the game board
def display_board(board):

    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if board[row][col] == 1:
                labeldisplay = "R"
            elif board[row][col] == 2:
                labeldisplay = "G"
            elif board[row][col] == 3:
                labeldisplay = "U"
            elif board[row][col] == 4:
                labeldisplay = "Y"
            elif board[row][col] == 5:
                labeldisplay = "B"
            elif board[row][col] == 6:
                labeldisplay = "P"
            elif board[row][col] == 7:
                labeldisplay = "S"
            tkinter.Label(gemframe, text=labeldisplay,
                          borderwidth=1).grid(row=row, column=col)

    for row in board:
        print('|' + '|'.join(GEM_COLORS[list(GEM_COLORS.keys())[gem - 1]] for gem in row) + '|')


# Define a function to swap two gems on the game board
def swap_gems(board, row1, col1, row2, col2):
    board[row1][col1], board[row2][col2] = board[row2][col2], board[row1][col1]


# Define a function to check if there are any matches on the game board
def find_matches(board, mana_board):
    global extra_turn, red_mana, green_mana, blue_mana, yellow_mana, brown_mana, purple_mana
    matches = set()
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            gem = board[row][col]
            if gem == 0:
                continue
            # match Ls and +
            if col < BOARD_SIZE - 2 and row < BOARD_SIZE - 2:
                # this is when the column is on the left side so there are 3 options |`` |-- and |__
                # |``
                if board[row][col + 1] == gem and board[row][col + 2] == gem and board[row + 1][col] == gem \
                        and board[row + 2][col] == gem:
                    # set the mana colors and stuff
                    matches.add((row, col))
                    matches.add((row, col + 1))
                    matches.add((row, col + 2))
                    matches.add((row + 1, col))
                    matches.add((row + 2, col))
                    mana_chain = max(mana_board[row][col], mana_board[row][col + 1], mana_board[row][col + 2],
                                     mana_board[row + 1][col], mana_board[row + 2][col])
                    if mana_chain == 0:
                        # bonus banner mana
                        # mastery bonus
                        if gem == 1:
                            red_mana += red_banner
                        elif gem == 2:
                            green_mana += green_banner
                        elif gem == 3:
                            blue_mana += blue_banner
                        elif gem == 4:
                            yellow_mana += yellow_banner
                        elif gem == 5:
                            brown_mana += brown_banner
                        elif gem == 6:
                            purple_mana += purple_banner

                    mana_chain = max(mana_chain, 2)
                    mana_board[row][col] = mana_chain
                    mana_board[row][col + 1] = mana_chain
                    mana_board[row][col + 2] = mana_chain
                    mana_board[row + 1][col] = mana_chain
                    mana_board[row + 2][col] = mana_chain

                    extra_turn = True
                # |--
                if board[row + 1][col + 1] == gem and board[row + 1][col + 2] == gem and board[row + 1][col] == gem \
                        and board[row + 2][col] == gem:
                    matches.add((row, col))
                    matches.add((row + 1, col + 1))
                    matches.add((row + 1, col + 2))
                    matches.add((row + 1, col))
                    matches.add((row + 2, col))
                    mana_chain = max(mana_board[row][col], mana_board[row + 1][col + 1], mana_board[row + 1][col + 2],
                                     mana_board[row + 1][col], mana_board[row + 2][col])
                    if mana_chain == 0:
                        # bonus banner mana
                        # mastery bonus
                        if gem == 1:
                            red_mana += red_banner
                        elif gem == 2:
                            green_mana += green_banner
                        elif gem == 3:
                            blue_mana += blue_banner
                        elif gem == 4:
                            yellow_mana += yellow_banner
                        elif gem == 5:
                            brown_mana += brown_banner
                        elif gem == 6:
                            purple_mana += purple_banner
                    mana_chain = max(mana_chain, 2)
                    mana_board[row][col] = mana_chain
                    mana_board[row + 1][col + 1] = mana_chain
                    mana_board[row + 1][col + 2] = mana_chain
                    mana_board[row + 1][col] = mana_chain
                    mana_board[row + 2][col] = mana_chain
                    extra_turn = True
                # |__
                if board[row + 2][col + 1] == gem and board[row + 2][col + 2] == gem and board[row + 1][col] == gem \
                        and board[row + 2][col] == gem:
                    matches.add((row, col))
                    matches.add((row + 2, col + 1))
                    matches.add((row + 2, col + 2))
                    matches.add((row + 1, col))
                    matches.add((row + 2, col))
                    mana_chain = max(mana_board[row][col], mana_board[row + 2][col + 1], mana_board[row + 2][col + 2],
                                     mana_board[row + 1][col], mana_board[row + 2][col])
                    if mana_chain == 0:
                        # bonus banner mana
                        # mastery bonus
                        if gem == 1:
                            red_mana += red_banner
                        elif gem == 2:
                            green_mana += green_banner
                        elif gem == 3:
                            blue_mana += blue_banner
                        elif gem == 4:
                            yellow_mana += yellow_banner
                        elif gem == 5:
                            brown_mana += brown_banner
                        elif gem == 6:
                            purple_mana += purple_banner
                    mana_chain = max(mana_chain, 2)

                    mana_board[row][col] = mana_chain
                    mana_board[row + 2][col + 1] = mana_chain
                    mana_board[row + 2][col + 2] = mana_chain
                    mana_board[row + 1][col] = mana_chain
                    mana_board[row + 2][col] = mana_chain
                    extra_turn = True

                if BOARD_SIZE - 1 > col > 0 and row < BOARD_SIZE - 2:
                    # then the next option is `|` -|- and _|_
                    # `|`
                    if board[row][col - 1] == gem and board[row][col + 1] == gem and board[row + 1][col] == gem \
                            and board[row + 2][col] == gem:
                        matches.add((row, col))
                        matches.add((row, col - 1))
                        matches.add((row, col + 1))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row][col - 1], mana_board[row][col + 1],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)

                        mana_board[row][col] = mana_chain
                        mana_board[row][col - 1] = mana_chain
                        mana_board[row][col + 1] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True
                    # -|-
                    if board[row + 1][col - 1] == gem and board[row + 1][col + 1] == gem \
                            and board[row + 1][col] == gem and board[row + 2][col] == gem:
                        matches.add((row, col))
                        matches.add((row + 1, col - 1))
                        matches.add((row + 1, col + 1))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row + 1][col - 1],
                                         mana_board[row + 1][col + 1],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)

                        mana_board[row][col] = mana_chain
                        mana_board[row + 1][col - 1] = mana_chain
                        mana_board[row + 1][col + 1] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True
                    # _|_
                    if board[row + 2][col - 1] == gem and board[row + 2][col + 1] == gem \
                            and board[row + 1][col] == gem and board[row + 2][col] == gem:
                        matches.add((row, col))
                        matches.add((row + 2, col - 1))
                        matches.add((row + 2, col + 1))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row + 2][col - 1],
                                         mana_board[row + 2][col + 1],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)

                        mana_board[row][col] = mana_chain
                        mana_board[row + 2][col - 1] = mana_chain
                        mana_board[row + 2][col + 1] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True
                if col > 1 and row < BOARD_SIZE - 2:
                    # final option is ``| --| and __|
                    # ``|
                    if board[row][col - 1] == gem and board[row][col - 2] == gem and board[row + 1][col] == gem \
                            and board[row + 2][col] == gem:
                        # set the mana colors and stuff
                        matches.add((row, col))
                        matches.add((row, col - 1))
                        matches.add((row, col - 2))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row][col - 1], mana_board[row][col - 2],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)

                        mana_board[row][col] = mana_chain
                        mana_board[row][col - 1] = mana_chain
                        mana_board[row][col - 2] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True
                    # --|
                    if board[row + 1][col - 1] == gem and board[row + 1][col - 2] == gem \
                            and board[row + 1][col] == gem and board[row + 2][col] == gem:
                        matches.add((row, col))
                        matches.add((row + 1, col - 1))
                        matches.add((row + 1, col - 2))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row + 1][col - 1],
                                         mana_board[row + 1][col - 2],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)
                        mana_board[row][col] = mana_chain
                        mana_board[row + 1][col - 1] = mana_chain
                        mana_board[row + 1][col - 2] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True
                    # _|_
                    if board[row + 2][col - 1] == gem and board[row + 2][col - 2] == gem \
                            and board[row + 1][col] == gem and board[row + 2][col] == gem:
                        matches.add((row, col))
                        matches.add((row + 2, col - 1))
                        matches.add((row + 2, col - 2))
                        matches.add((row + 1, col))
                        matches.add((row + 2, col))
                        mana_chain = max(mana_board[row][col], mana_board[row + 2][col - 1],
                                         mana_board[row + 2][col - 2],
                                         mana_board[row + 1][col], mana_board[row + 2][col])
                        if mana_chain == 0:
                            # bonus banner mana
                            # mastery bonus
                            if gem == 1:
                                red_mana += red_banner
                            elif gem == 2:
                                green_mana += green_banner
                            elif gem == 3:
                                blue_mana += blue_banner
                            elif gem == 4:
                                yellow_mana += yellow_banner
                            elif gem == 5:
                                brown_mana += brown_banner
                            elif gem == 6:
                                purple_mana += purple_banner
                        mana_chain = max(mana_chain, 2)
                        mana_board[row][col] = mana_chain
                        mana_board[row + 2][col - 1] = mana_chain
                        mana_board[row + 2][col - 2] = mana_chain
                        mana_board[row + 1][col] = mana_chain
                        mana_board[row + 2][col] = mana_chain
                        extra_turn = True

            # match 5s
            if col < BOARD_SIZE - 4 and board[row][col + 1] == gem and board[row][col + 2] == gem \
                    and board[row][col + 3] == gem and board[row][col + 4] == gem:
                matches.add((row, col))
                matches.add((row, col + 1))
                matches.add((row, col + 2))
                matches.add((row, col + 3))
                matches.add((row, col + 4))
                mana_chain = max(mana_board[row][col], mana_board[row][col + 1], mana_board[row][col + 2],
                                 mana_board[row][col + 3], mana_board[row][col + 4])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                    elif gem == 2:
                        green_mana += green_banner
                    elif gem == 3:
                        blue_mana += blue_banner
                    elif gem == 4:
                        yellow_mana += yellow_banner
                    elif gem == 5:
                        brown_mana += brown_banner
                    elif gem == 6:
                        purple_mana += purple_banner
                mana_chain = max(mana_chain, 2)
                mana_board[row][col] = mana_chain
                mana_board[row][col + 1] = mana_chain
                mana_board[row][col + 2] = mana_chain
                mana_board[row][col + 3] = mana_chain
                mana_board[row][col + 4] = mana_chain
                extra_turn = True
            if row < BOARD_SIZE - 4 and board[row + 1][col] == gem and board[row + 2][col] == gem \
                    and board[row + 3][col] == gem and board[row + 4][col] == gem:
                matches.add((row, col))
                matches.add((row + 1, col))
                matches.add((row + 2, col))
                matches.add((row + 3, col))
                matches.add((row + 4, col))
                mana_chain = max(mana_board[row][col], mana_board[row + 1][col], mana_board[row + 2][col],
                                 mana_board[row + 3][col], mana_board[row + 4][col])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                    elif gem == 2:
                        green_mana += green_banner
                    elif gem == 3:
                        blue_mana += blue_banner
                    elif gem == 4:
                        yellow_mana += yellow_banner
                    elif gem == 5:
                        brown_mana += brown_banner
                    elif gem == 6:
                        purple_mana += purple_banner
                mana_chain = max(mana_chain, 2)
                mana_board[row][col] = mana_chain
                mana_board[row + 1][col] = mana_chain
                mana_board[row + 2][col] = mana_chain
                mana_board[row + 3][col] = mana_chain
                mana_board[row + 4][col] = mana_chain
                extra_turn = True
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            gem = board[row][col]
            if gem == 0:
                continue
            # match 4s
            if col < BOARD_SIZE - 3 and board[row][col + 1] == gem and board[row][col + 2] == gem \
                    and board[row][col + 3] == gem:
                matches.add((row, col))
                matches.add((row, col + 1))
                matches.add((row, col + 2))
                matches.add((row, col + 3))
                mana_chain = max(mana_board[row][col], mana_board[row][col + 1], mana_board[row][col + 2],
                                 mana_board[row][col + 3])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                    elif gem == 2:
                        green_mana += green_banner
                    elif gem == 3:
                        blue_mana += blue_banner
                    elif gem == 4:
                        yellow_mana += yellow_banner
                    elif gem == 5:
                        brown_mana += brown_banner
                    elif gem == 6:
                        purple_mana += purple_banner
                mana_chain = max(mana_chain, 1)
                mana_board[row][col] = mana_chain
                mana_board[row][col + 1] = mana_chain
                mana_board[row][col + 2] = mana_chain
                mana_board[row][col + 3] = mana_chain

                extra_turn = True
            if row < BOARD_SIZE - 3 and board[row + 1][col] == gem and board[row + 2][col] == gem \
                    and board[row + 3][col] == gem:
                matches.add((row, col))
                matches.add((row + 1, col))
                matches.add((row + 2, col))
                matches.add((row + 3, col))
                mana_chain = max(mana_board[row][col], mana_board[row + 1][col], mana_board[row + 2][col],
                                 mana_board[row + 3][col])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                    elif gem == 2:
                        green_mana += green_banner
                    elif gem == 3:
                        blue_mana += blue_banner
                    elif gem == 4:
                        yellow_mana += yellow_banner
                    elif gem == 5:
                        brown_mana += brown_banner
                    elif gem == 6:
                        purple_mana += purple_banner
                mana_chain = max(mana_chain, 1)
                mana_board[row][col] = mana_chain
                mana_board[row + 1][col] = mana_chain
                mana_board[row + 2][col] = mana_chain
                mana_board[row + 3][col] = mana_chain
                extra_turn = True
            if col < BOARD_SIZE - 2 and board[row][col + 1] == gem and board[row][col + 2] == gem:
                matches.add((row, col))
                matches.add((row, col + 1))
                matches.add((row, col + 2))
                mana_chain = max(mana_board[row][col], mana_board[row][col + 1], mana_board[row][col + 2])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                        if random.randint(1, 100) < red_mastery_chance:
                            red_mana += 3
                    elif gem == 2:
                        green_mana += green_banner
                        if random.randint(1, 100) < green_mastery_chance:
                            green_mana += 3
                    elif gem == 3:
                        blue_mana += blue_banner
                        if random.randint(1, 100) < blue_mastery_chance:
                            blue_mana += 3
                    elif gem == 4:
                        yellow_mana += yellow_banner
                        if random.randint(1, 100) < yellow_mastery_chance:
                            yellow_mana += 3
                    elif gem == 5:
                        brown_mana += brown_banner
                        if random.randint(1, 100) < brown_mastery_chance:
                            brown_mana += 3
                    elif gem == 6:
                        purple_mana += purple_banner
                        if random.randint(1, 100) < purple_mastery_chance:
                            purple_mana += 3
                mana_chain = max(mana_chain, 1)
                mana_board[row][col] = mana_chain
                mana_board[row][col + 1] = mana_chain
                mana_board[row][col + 2] = mana_chain
            if row < BOARD_SIZE - 2 and board[row + 1][col] == gem and board[row + 2][col] == gem:
                matches.add((row, col))
                matches.add((row + 1, col))
                matches.add((row + 2, col))
                mana_chain = max(mana_board[row][col], mana_board[row + 1][col], mana_board[row + 2][col])
                if mana_chain == 0:
                    # bonus banner mana
                    # mastery bonus
                    if gem == 1:
                        red_mana += red_banner
                        if random.randint(1, 100) < red_mastery_chance:
                            red_mana += 3
                    elif gem == 2:
                        green_mana += green_banner
                        if random.randint(1, 100) < green_mastery_chance:
                            green_mana += 3
                    elif gem == 3:
                        blue_mana += blue_banner
                        if random.randint(1, 100) < blue_mastery_chance:
                            blue_mana += 3
                    elif gem == 4:
                        yellow_mana += yellow_banner
                        if random.randint(1, 100) < yellow_mastery_chance:
                            yellow_mana += 3
                    elif gem == 5:
                        brown_mana += brown_banner
                        if random.randint(1, 100) < brown_mastery_chance:
                            brown_mana += 3
                    elif gem == 6:
                        purple_mana += purple_banner
                        if random.randint(1, 100) < purple_mastery_chance:
                            purple_mana += 3

                mana_chain = max(mana_chain, 1)
                mana_board[row][col] = mana_chain
                mana_board[row + 1][col] = mana_chain
                mana_board[row + 2][col] = mana_chain

    return matches


# Define a function to remove gems from the game board
def remove_gems(board, matches, mana_board):
    global red_mana
    global green_mana
    global blue_mana
    global yellow_mana
    global brown_mana
    global purple_mana

    for row, col in matches:
        if board[row][col] == 0:
            print("0")
        elif board[row][col] == 1:
            red_mana += mana_board[row][col]
            # print("Red" + str(red_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 2:
            green_mana += mana_board[row][col]
            # print("Green" + str(red_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 3:
            blue_mana += mana_board[row][col]
            # print("Blue" + str(blue_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 4:
            yellow_mana += mana_board[row][col]
            # print("Yellow" + str(yellow_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 5:
            brown_mana += mana_board[row][col]
            # print("Brown" + str(brown_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 6:
            purple_mana += mana_board[row][col]
            # print("Purple" + str(purple_mana))
            mana_board[row][col] = 0
        elif board[row][col] == 7:
            # print("Skull")
            mana_board[row][col] = 0
        board[row][col] = 0
    shift_down(board)


def shift_down(board):
    for row in range(BOARD_SIZE - 1, 0, -1):
        for col in range(BOARD_SIZE):
            if row > 0 and board[row][col] == 0:
                for i in range(row - 1, -1, -1):
                    if board[i][col] != 0:
                        board[row][col] = board[i][col]
                        board[i][col] = 0
                        break


def fill_blanks(board):
    global blue_storm
    global green_storm
    global red_storm
    global yellow_storm
    global purple_storm
    global brown_storm
    global bone_storm
    # priority is blue, green, red, yellow, purple, brown
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if board[row][col] == 0:
                board[row][col] = random.randint(1, NUM_GEM_TYPES)
                if brown_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 5
                if purple_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 6
                if yellow_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 4
                if red_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 1
                if green_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 2
                if blue_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 3
                if bone_storm:
                    if random.randint(1, 100) <= 15:
                        board[row][col] = 7


def add_color_gems(board, number_to_add, color_gem_to_add):
    # adds brown gems for now
    num_brown_gems_added = 0
    while num_brown_gems_added < number_to_add:
        row = random.randint(0, BOARD_SIZE - 1)
        col = random.randint(0, BOARD_SIZE - 1)
        if board[row][col] != color_gem_to_add:
            board[row][col] = color_gem_to_add
            num_brown_gems_added += 1


def check_four_matches(board):
    # save the input board as the initial board
    # don't actually change the initial board and just try permutations on a temporary board
    initial_board = board
    # try all the possible swaps and check for 4-matches
    # check swapping left and right gems first
    global extra_turn, labeldisplay, infoTextVar
    good_swaps = list()
    good_swaps_target = list()
    great_swaps = list()
    great_swaps_target = list()
    good_converts = list()
    great_converts = list()
    good_double_converts = list()
    great_double_converts = list()
    good_removal = list()
    great_removal = list()
    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if row < (BOARD_SIZE - 1):
                board_temporary = copy.deepcopy(initial_board)
                mana_board = create_mana_board()
                swap_gems(board_temporary, row, col, row + 1, col)
                first_match = True
                while True:
                    matches = find_matches(board_temporary, mana_board)
                    if matches:
                        # check if the first match has an extra turn (which would guarantee an extra vs a cascade extra which might not guarantee)
                        if first_match:
                            if extra_turn:
                                great_swaps.append((row, col))
                                great_swaps_target.append((row + 1,col))
                                extra_turn = False
                            first_match = False
                        # remove and shift down gems
                        remove_gems(board_temporary, matches, mana_board)
                    # then loop again and look for more matches
                    else:
                        # no matches so break and check if there was an extra turn
                        break
                if extra_turn:
                    good_swaps.append((row, col))
                    good_swaps_target.append((row + 1, col))
                    extra_turn = False
            if col < (BOARD_SIZE - 1):
                board_temporary = copy.deepcopy(initial_board)
                mana_board = create_mana_board()
                swap_gems(board_temporary, row, col, row, col + 1)
                first_match = True
                while True:
                    matches = find_matches(board_temporary, mana_board)
                    if matches:
                        if first_match:
                            if extra_turn:
                                great_swaps.append((row, col))
                                great_swaps_target.append((row, col + 1))
                                extra_turn = False
                            first_match = False
                        # remove and shift down gems
                        remove_gems(board_temporary, matches, mana_board)
                    # then loop again and look for more matches
                    else:
                        # no matches so break and check if there was an extra turn
                        break
                if extra_turn:
                    good_swaps.append((row, col))
                    good_swaps_target.append((row, col + 1))
                    extra_turn = False
    # Check the color converters single converters
    # a reminder of color order
    # red:1, green 2, blue 3, yellow 4, brown 5, purple 6, skull 7
    for color_initial in range(1, 8):
        for color_secondary in range(1, 8):
            board_temporary = copy.deepcopy(initial_board)
            mana_board = create_mana_board()
            for row in range(BOARD_SIZE):
                for col in range(BOARD_SIZE):
                    if board_temporary[row][col] == color_initial:
                        board_temporary[row][col] = color_secondary
            first_match = True
            while True:
                matches = find_matches(board_temporary, mana_board)
                if matches:
                    if first_match:
                        if extra_turn:
                            great_converts.append((color_initial, color_secondary))
                            extra_turn = False
                        first_match = False
                    # remove and shift down gems
                    remove_gems(board_temporary, matches, mana_board)
                # then loop again and look for more matches
                else:
                    # no matches so break and check if there was an extra turn
                    break
            if extra_turn:
                good_converts.append((color_initial, color_secondary))
                extra_turn = False
    # check color removal astral spirit
    for color in range(1, 8):
        board_temporary = copy.deepcopy(initial_board)
        mana_board = create_mana_board()
        for row in range(BOARD_SIZE):
            for col in range(BOARD_SIZE):
                if board_temporary[row][col] == color:
                    board_temporary[row][col] = 0
                    shift_down(board_temporary)
        first_match = True
        while True:
            matches = find_matches(board_temporary, mana_board)
            if matches:
                if first_match:
                    if extra_turn:
                        great_removal.append((color))
                        extra_turn = False
                    first_match = False
                # remove and shift down gems
                remove_gems(board_temporary, matches, mana_board)
            # then loop again and look for more matches
            else:
                # no matches so break and check if there was an extra turn
                break
        if extra_turn:
            good_removal.append((color))
            extra_turn = False

    # Check double color converters
    # Check the color converters single converters
    # a reminder of color order
    # red:1, green 2, blue 3, yellow 4, brown 5, purple 6, skull 7
    for first_initial in range(1, 8):
        for first_target in range(1, 8):
            for second_initial in range(1, 8):
                for second_target in range(1, 8):
                    board_temporary = copy.deepcopy(initial_board)
                    mana_board = create_mana_board()
                    for row in range(BOARD_SIZE):
                        for col in range(BOARD_SIZE):
                            if board_temporary[row][col] == first_initial:
                                board_temporary[row][col] = first_target
                            if board_temporary[row][col] == second_initial:
                                board_temporary[row][col] = second_target
                    first_match = True
                    while True:
                        matches = find_matches(board_temporary, mana_board)
                        if matches:
                            if first_match:
                                if extra_turn:
                                    great_double_converts.append((first_initial, first_target,second_initial,second_target))
                                    extra_turn = False
                                first_match = False
                            # remove and shift down gems
                            remove_gems(board_temporary, matches, mana_board)
                        # then loop again and look for more matches
                        else:
                            # no matches so break and check if there was an extra turn
                            break
                    if extra_turn:
                        good_double_converts.append((first_initial, first_target,second_initial,second_target))
                        extra_turn = False



    for row in range(BOARD_SIZE):
        for col in range(BOARD_SIZE):
            if (row, col) in great_swaps:
                backgroundcolor = "Green"
            elif (row, col) in great_swaps_target:
                backgroundcolor = "Light Green"
            elif (row, col) in good_swaps:
                backgroundcolor = "Yellow"
            elif (row, col) in good_swaps_target:
                backgroundcolor = "Light Yellow"
            else:
                backgroundcolor = "White"
            if board[row][col] == 1:
                labeldisplay = "R"
            elif board[row][col] == 2:
                labeldisplay = "G"
            elif board[row][col] == 3:
                labeldisplay = "U"
            elif board[row][col] == 4:
                labeldisplay = "Y"
            elif board[row][col] == 5:
                labeldisplay = "B"
            elif board[row][col] == 6:
                labeldisplay = "P"
            elif board[row][col] == 7:
                labeldisplay = "S"
            elif board[row][col] == 8:
                labeldisplay = "X"
                if (row, col) in great_swaps:
                    backgroundcolor = "Green"
                elif (row, col) in good_swaps:
                    backgroundcolor = "Yellow"
                else:
                    backgroundcolor="Red"
            elif board[row][col] == 0:
                labeldisplay = " "
            tkinter.Label(gemframe, text=labeldisplay,
                          borderwidth=1, background=backgroundcolor).grid(row=row, column=col)

    #infoframe = tkinter.Frame(window)
    #infoframe.pack(side=tkinter.RIGHT)
    infoText = "Possible Extras:"
    if (great_swaps):
        infoText = infoText + ("\n4 match extra")
    if (good_swaps):
        infoText = infoText + ("\nCascade into an extra")
    if (verboseoutput):

        if (great_removal):
            if 1 in great_removal:
                infoText = infoText + ("\n") + ("Remove red")
            if 2 in great_removal:
                infoText = infoText + ("\n") + ("Remove green")
            if 3 in great_removal:
                infoText = infoText + ("\n") + ("Remove blue")
            if 4 in great_removal:
                infoText = infoText + ("\n") + ("Remove yellow")
            if 5 in great_removal:
                infoText = infoText + ("\n") + ("Remove brown")
            if 6 in great_removal:
                infoText = infoText + ("\n") + ("Remove purple")
            if 7 in great_removal:
                infoText = infoText + ("\n") + ("Remove skull")
        if (good_removal):
            if 1 in good_removal:
                infoText = infoText + ("\n") + ("Remove red (Cascade)")
            if 2 in good_removal:
                infoText = infoText + ("\n") + ("Remove green (Cascade)")
            if 3 in good_removal:
                infoText = infoText + ("\n") + ("Remove blue (Cascade)")
            if 4 in good_removal:
                infoText = infoText + ("\n") + ("Remove yellow (Cascade)")
            if 5 in good_removal:
                infoText = infoText + ("\n") + ("Remove brown (Cascade)")
            if 6 in good_removal:
                infoText = infoText + ("\n") + ("Remove purple (Cascade)")
            if 7 in good_removal:
                infoText = infoText + ("\n") + ("Remove skull (Cascade)")
        for converts in great_converts:
            if converts[0]==1:
                infoText = infoText + ("\n") + ("Convert from red to ")
            if converts[0]==2:
                infoText = infoText + ("\n") + ("Convert from green to ")
            if converts[0]==3:
                infoText = infoText + ("\n") + ("Convert from blue to ")
            if converts[0]==4:
                infoText = infoText + ("\n") + ("Convert from yellow to ")
            if converts[0]==5:
                infoText = infoText + ("\n") + ("Convert from brown to ")
            if converts[0]==6:
                infoText = infoText + ("\n") + ("Convert from purple to ")
            if converts[0]==7:
                infoText = infoText + ("\n") + ("Convert from skull to ")
            if converts[1]==1:
                infoText = infoText + ("red")
            if converts[1]==2:
                infoText = infoText + ("green")
            if converts[1]==3:
                infoText = infoText + ("blue")
            if converts[1]==4:
                infoText = infoText + ("yellow")
            if converts[1]==5:
                infoText = infoText + ("brown")
            if converts[1]==6:
                infoText = infoText + ("purple")
            if converts[1]==7:
                infoText = infoText + ("skull")
        for converts in good_converts:
            if converts[0]==1:
                infoText = infoText + ("\n") + ("Convert from red to ")
            if converts[0]==2:
                infoText = infoText + ("\n") + ("Convert from green to ")
            if converts[0]==3:
                infoText = infoText + ("\n") + ("Convert from blue to ")
            if converts[0]==4:
                infoText = infoText + ("\n") + ("Convert from yellow to ")
            if converts[0]==5:
                infoText = infoText + ("\n") + ("Convert from brown to ")
            if converts[0]==6:
                infoText = infoText + ("\n") + ("Convert from purple to ")
            if converts[0]==7:
                infoText = infoText + ("\n") + ("Convert from skull to ")
            if converts[1]==1:
                infoText = infoText + ("red (Cascade)")
            if converts[1]==2:
                infoText = infoText + ("green (Cascade)")
            if converts[1]==3:
                infoText = infoText + ("blue (Cascade)")
            if converts[1]==4:
                infoText = infoText + ("yellow (Cascade)")
            if converts[1]==5:
                infoText = infoText + ("brown (Cascade)")
            if converts[1]==6:
                infoText = infoText + ("purple (Cascade)")
            if converts[1]==7:
                infoText = infoText + ("skull (Cascade)")
    else:
        if draxxius.get():
            if (5,7) in great_converts:
                infoText = infoText + ("\n") + ("Draxxius convert")
            if (5,7) in good_converts:
                infoText = infoText + ("\n") + ("Draxxius convert (Cascade)")
        if taipan.get():
            if (5,1) in great_converts:
                infoText = infoText + ("\n") + ("Tai Pan convert")
            if (5,1) in good_converts:
                infoText = infoText + ("\n") + ("Tai Pan convert (Cascade)")
        if eldritchminion.get():
            if (5,6) in great_converts:
                infoText = infoText + ("\n") + ("Eldritch Minion convert")
            if (5,6) in good_converts:
                infoText = infoText + ("\n") + ("Eldritch Minion convert (Cascade)")
        if baneofmercy.get():
            if (4,6) in great_converts:
                infoText = infoText + ("\n") + ("Bane of Mercy convert")
            if (4,6) in good_converts:
                infoText = infoText + ("\n") + ("Bane of Mercy convert (Cascade)")
        if mercy.get():
            if (6,4) in great_converts:
                infoText = infoText + ("\n") + ("Mercy convert")
            if (6,4) in good_converts:
                infoText = infoText + ("\n") + ("Mercy convert (Cascade)")
        if veneratus.get():
            if (1,4) in great_converts:
                infoText = infoText + ("\n") + ("Veneratus convert")
            if (1,4) in good_converts:
                infoText = infoText + ("\n") + ("Veneratus convert (Cascade)Dragon gems don't work yet ignore this")
        if malcandessa.get():
            if (4,2) in great_converts:
                infoText = infoText + ("\n") + ("Malcandessa convert")
            if (4,2) in good_converts:
                infoText = infoText + ("\n") + ("Malcandessa convert (Cascade)")
        if moonsinger.get():
            if (6,2) in great_converts:
                infoText = infoText + ("\n") + ("Moon Singer convert")
            if (6,2) in good_converts:
                infoText = infoText + ("\n") + ("Moon Singer convert (Cascade)")
        if apophisis.get():
            if (2,7) in great_converts:
                infoText = infoText + ("\n") + ("Apophisis convert")
            if (2,7) in good_converts:
                infoText = infoText + ("\n") + ("Apophisis convert (Cascade)")
        if trknala.get():
            if (2,7) in great_converts:
                infoText = infoText + ("\n") + ("TrkNala convert")
            if (2,7) in good_converts:
                infoText = infoText + ("\n") + ("TrkNala convert (Cascade)")
        if gimletstormbrew.get():
            if (2,5) in great_converts:
                infoText = infoText + ("\n") + ("Gimlet Stormbrew convert")
            if (2,5) in good_converts:
                infoText = infoText + ("\n") + ("Gimlet Stormbrew convert (Cascade)")
        if graveseer.get():
            if (2,6) in great_converts:
                infoText = infoText + ("\n") + ("Grave Seer convert")
            if (2,6) in good_converts:
                infoText = infoText + ("\n") + ("Grave Seer convert (Cascade)")
        if lamashtu.get():
            if (4,6) in great_converts:
                infoText = infoText + ("\n") + ("Lamashtu convert")
            if (4,6) in good_converts:
                infoText = infoText + ("\n") + ("Lamashtu convert (Cascade)")
        if scurvyseadog.get():
            if (2,3) in great_converts:
                infoText = infoText + ("\n") + ("Scurvy Seadog convert")
            if (2,3) in good_converts:
                infoText = infoText + ("\n") + ("Scurvy Seadog convert (Cascade)")
        if inari.get():
            if (2,3) in great_converts:
                infoText = infoText + ("\n") + ("Inari convert")
            if (2,3) in good_converts:
                infoText = infoText + ("\n") + ("Inari convert (Cascade)")
        if childofsummer.get():
            if (5,1) in great_converts:
                infoText = infoText + ("\n") + ("Child of Summer convert")
            if (5,1) in good_converts:
                infoText = infoText + ("\n") + ("Child of Summer convert (Cascade)")
        if firstmateaxelubber.get():
            if (3,1) in great_converts:
                infoText = infoText + ("\n") + ("First Mate Axelubber convert")
            if (3,1) in good_converts:
                infoText = infoText + ("\n") + ("First Mate Axelubber convert (Cascade)")
        if moonrabbit.get():
            if (3,4) in great_converts:
                infoText = infoText + ("\n") + ("Moon Rabbit convert")
            if (3,4) in good_converts:
                infoText = infoText + ("\n") + ("Moon Rabbit convert (Cascade)")
        if spiderthrone.get():
            if (5,6) in great_converts:
                infoText = infoText + ("\n") + ("Spider Throne convert")
            if (5,6) in good_converts:
                infoText = infoText + ("\n") + ("Spider Throne convert (Cascade)")
        if fistofzorn.get():
            if (4,7) in great_converts:
                infoText = infoText + ("\n") + ("Fist of Zorn convert")
            if (4,7) in good_converts:
                infoText = infoText + ("\n") + ("Fist of Zorn convert (Cascade)")
        if drownedsailor.get():
            if (4,3) in great_converts:
                infoText = infoText + ("\n") + ("Drowned Sailor convert")
            if (4,3) in good_converts:
                infoText = infoText + ("\n") + ("Drowned Sailor convert (Cascade)")
        if daughterofice.get():
            if (1,3) in great_converts:
                infoText = infoText + ("\n") + ("Daughter of Ice convert")
            if (1,3) in good_converts:
                infoText = infoText + ("\n") + ("Daughter of Ice convert (Cascade)")
        if heraldofdamnation.get():
            if (3,6) in great_converts:
                infoText = infoText + ("\n") + ("Herald of Damnation convert")
            if (3,6) in good_converts:
                infoText = infoText + ("\n") + ("Herald of Damnation convert (Cascade)")
        if yaoguai.get():
            if (6,1) in great_converts:
                infoText = infoText + ("\n") + ("Yao Guai convert")
            if (6,1) in good_converts:
                infoText = infoText + ("\n") + ("Yao Guai convert (Cascade)")
        if doomedaxe.get():
            if (6,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Axe convert")
            if (6,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Axe convert (Cascade)Doom skulls don't work")
        if doomedclub.get():
            if (2,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Club convert")
            if (2,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Club convert (Cascade)Doom skulls don't work")
        if doomedblade.get():
            if (1,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Blade convert")
            if (1,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Blade convert (Cascade)Doom skulls don't work")
        if doomedglaive.get():
            if (5,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Glaive convert")
            if (5,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Glaive convert (Cascade)Doom skulls don't work")
        if doomedscythe.get():
            if (4,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Scythe convert")
            if (4,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Scythe convert (Cascade)Doom skulls don't work")
        if doomedcrossbow.get():
            if (3,7) in great_converts:
                infoText = infoText + ("\n") + ("Doomed Crossbow convert")
            if (3,7) in good_converts:
                infoText = infoText + ("\n") + ("Doomed Crossbow convert (Cascade)Doom skulls don't work")
        if astralspirit.get():
            if (great_removal):
                infoText = infoText + ("\n") + ("Use Astral Spirit to")
                if 1 in great_removal:
                    infoText = infoText + ("\n") + ("Remove red")
                if 2 in great_removal:
                    infoText = infoText + ("\n") + ("Remove green")
                if 3 in great_removal:
                    infoText = infoText + ("\n") + ("Remove blue")
                if 4 in great_removal:
                    infoText = infoText + ("\n") + ("Remove yellow")
                if 5 in great_removal:
                    infoText = infoText + ("\n") + ("Remove brown")
                if 6 in great_removal:
                    infoText = infoText + ("\n") + ("Remove purple")
                if 7 in great_removal:
                    infoText = infoText + ("\n") + ("Remove skull")
            if (good_removal):
                infoText = infoText + ("\n") + ("Use Astral Spirit to")
                if 1 in good_removal:
                    infoText = infoText + ("\n") + ("Remove red (Cascade)")
                if 2 in good_removal:
                    infoText = infoText + ("\n") + ("Remove green (Cascade)")
                if 3 in good_removal:
                    infoText = infoText + ("\n") + ("Remove blue (Cascade)")
                if 4 in good_removal:
                    infoText = infoText + ("\n") + ("Remove yellow (Cascade)")
                if 5 in good_removal:
                    infoText = infoText + ("\n") + ("Remove brown (Cascade)")
                if 6 in good_removal:
                    infoText = infoText + ("\n") + ("Remove purple (Cascade)")
                if 7 in good_removal:
                    infoText = infoText + ("\n") + ("Remove skull (Cascade)")
        if spiritfox.get():
            if 4 in great_removal:
                infoText = infoText + ("\n") + ("Use Spirit Fox to remove yellow")
            if 4 in good_removal:
                infoText = infoText + ("\n") + ("Use Spirit Fox to remove yellow (Cascade)")




    if ((2,4,1,7) in great_double_converts)& divineishbaala.get():
        infoText = infoText + ("\n") + ("Divine Ishbaala extra")
    if ((2,4,1,7) in good_double_converts)& divineishbaala.get():
        infoText = infoText + ("\n") + ("Divine Ishbaala extra (Cascade)")
    if ((1,6,4,7) in great_double_converts)& alderfather.get():
        infoText = infoText + ("\n") + ("Alderfather extra")
    if ((1,6,4,7) in good_double_converts)& alderfather.get():
        infoText = infoText + ("\n") + ("Alderfather extra (Cascade)")
    if ((1, 6, 4, 7) in great_double_converts) & nimue.get():
        infoText = infoText + ("\n") + ("Nimue extra")
    if ((1, 6, 4, 7) in good_double_converts) & nimue.get():
        infoText = infoText + ("\n") + ("Nimue extra (Cascade)")
    if ((5, 2, 6, 7) in great_double_converts) & forestguardian.get():
        infoText = infoText + ("\n") + ("Forest Guardian extra")
    if ((5, 2, 6, 7) in good_double_converts) & forestguardian.get():
        infoText = infoText + ("\n") + ("Forest Guardian extra (Cascade)")
    if ((6, 1, 5, 7) in great_double_converts) & qilin.get():
        infoText = infoText + ("\n") + ("Qilin extra")
    if ((6, 1, 5, 7) in good_double_converts) & qilin.get():
        infoText = infoText + ("\n") + ("Qilin extra (Cascade)")
    if ((5, 4, 3, 7) in great_double_converts) & sekhma.get():
        infoText = infoText + ("\n") + ("Sekhma extra")
    if ((5, 4, 3, 7) in good_double_converts) & sekhma.get():
        infoText = infoText + ("\n") + ("Sekhma extra (Cascade)")
    if ((3, 1, 5, 7) in great_double_converts) & beastmastertorbern.get():
        infoText = infoText + ("\n") + ("Beast Master Torbern extra")
    if ((3, 1, 5, 7) in good_double_converts) & beastmastertorbern.get():
        infoText = infoText + ("\n") + ("Beast Master Torbern extra (Cascade)")
    if ((5, 1, 2, 7) in great_double_converts) & infernalking.get():
        infoText = infoText + ("\n") + ("Infernal King extra")
    if ((5, 1, 2, 7) in good_double_converts) & infernalking.get():
        infoText = infoText + ("\n") + ("Infernal King extra (Cascade)")
    if ((6, 4, 2, 7) in great_double_converts) & sirquentinhadley.get():
        infoText = infoText + ("\n") + ("Sir Quentin Hadley extra")
    if ((6, 4, 2, 7) in good_double_converts) & sirquentinhadley.get():
        infoText = infoText + ("\n") + ("Sir Quentin Hadley extra (Cascade)")
    if ((3, 5, 4, 7) in great_double_converts) & wrath.get():
        infoText = infoText + ("\n") + ("Wrath extra")
    if ((3, 5, 4, 7) in good_double_converts) & wrath.get():
        infoText = infoText + ("\n") + ("Wrath extra (Cascade)")
    if ((4, 3, 1, 7) in great_double_converts) & glaycion.get():
        infoText = infoText + ("\n") + ("Glaycion extra")
    if ((4, 3, 1, 7) in good_double_converts) & glaycion.get():
        infoText = infoText + ("\n") + ("Glaycion extra (Cascade)")

    print(infoText)
    print(great_double_converts)
    infoTextVar.set(infoText)

    window.mainloop()
    print(great_swaps)


# Define the main game loop
def play_game():
    global red_mana
    global green_mana
    global blue_mana
    global yellow_mana
    global brown_mana
    global purple_mana
    global extra_turn
    # setting storm
    global brown_storm
    global yellow_storm
    global blue_storm
    global red_storm
    global green_storm
    global purple_storm
    global bone_storm
    global red_banner, green_banner, blue_banner, yellow_banner, brown_banner, purple_banner
    global red_mastery_chance, green_mastery_chance, blue_mastery_chance
    global yellow_mastery_chance, brown_mastery_chance, purple_mastery_chance
    red_mastery_chance = 66
    green_mastery_chance = 66
    blue_mastery_chance = 66
    yellow_mastery_chance = 66
    brown_mastery_chance = 66
    purple_mastery_chance = 66
    troop_one_mana = 0
    troop_one_max_mana = 16
    troop_two_mana = 0
    troop_two_max_mana = 16
    bone_storm = False
    brown_storm = False
    green_storm = True
    green_banner = 2
    brown_banner = 1
    loop_ends_no_mana = 0
    loop_ends_no_extra = 0
    number_of_gems_being_created = 15
    results = []
    iterations = 0
    second_troop_results = []
    second_troop_casts = 0
    board = create_board()
    mana_board = create_mana_board()
    enemy_turn_check_matches(board, mana_board)
    while True:
        # display_board(board)
        matches = find_matches(board, mana_board)
        # print("is extra turn" + str(extra_turn))
        if matches:
            remove_gems(board, matches, mana_board)
            fill_blanks(board)

            # check if the troops have mana to cast their spells
            # assume it goes blue->green->red->yellow->purple->brown
            # 1venoxia/2bee
            #
            # if troop_two_mana < troop_two_max_mana:
            #     if troop_two_mana + blue_mana > troop_two_max_mana:
            #         blue_mana = blue_mana-(troop_two_max_mana-troop_two_mana)
            #         troop_two_mana = troop_two_max_mana
            #     else:
            #         troop_two_mana += blue_mana
            #         blue_mana = 0

            # if troop_one_mana < troop_one_max_mana:
            #     if troop_one_mana + green_mana > troop_one_max_mana:
            #         green_mana = green_mana - (troop_one_max_mana - troop_one_mana)
            #         troop_one_mana = troop_one_max_mana
            #     else:
            #         troop_one_mana += green_mana
            #         green_mana = 0
            if troop_two_mana < troop_two_max_mana:
                if troop_two_mana + blue_mana > troop_two_max_mana:
                    blue_mana = blue_mana - (troop_two_max_mana - troop_two_mana)
                    troop_two_mana = troop_two_max_mana
                else:
                    troop_two_mana += blue_mana
                    blue_mana = 0
            if troop_two_mana < troop_two_max_mana:
                if troop_two_mana + green_mana > troop_two_max_mana:
                    green_mana = green_mana - (troop_two_max_mana - troop_two_mana)
                    troop_two_mana = troop_two_max_mana
                else:
                    troop_two_mana += green_mana
                    green_mana = 0
            if troop_one_mana < troop_one_max_mana:
                if troop_one_mana + brown_mana > troop_one_max_mana:
                    brown_mana = brown_mana - (troop_one_max_mana - troop_one_mana)
                    troop_one_mana = troop_one_max_mana
                else:
                    troop_one_mana += brown_mana
                    brown_mana = 0
            if troop_one_mana < troop_one_max_mana:
                if troop_one_mana + purple_mana > troop_one_max_mana:
                    purple_mana = purple_mana - (troop_one_max_mana - troop_one_mana)
                    troop_one_mana = troop_one_max_mana
                else:
                    troop_one_mana += purple_mana
                    purple_mana = 0
            # at end of adding mana to troops set all extra mana to 0
            red_mana = 0
            green_mana = 0
            blue_mana = 0
            yellow_mana = 0
            brown_mana = 0
            purple_mana = 0

        if len(matches) > 0:
            print('Matches found: ', matches)
        else:
            print('No matches found.')
            if iterations == 0:
                # this is the first one so first cast of veno
                print('first iteration')
                iterations += 1
                add_color_gems(board, 9, 2)
                add_color_gems(board, 9, 5)
                troop_two_mana = 0
                if random.randint(1, 100) < 40:
                    extra_turn = True
                if random.randint(1, 100) < 40:
                    troop_two_mana = 8
                continue
            if extra_turn:
                print('there is an extra turn')
                extra_turn = False
                if troop_two_mana >= troop_two_max_mana:
                    print('there is enough for another veno cast')
                    iterations += 1
                    # cast bee here
                    add_color_gems(board, 9, 2)
                    add_color_gems(board, 9, 5)
                    troop_two_mana = 0
                    if random.randint(1, 100) < 40:
                        extra_turn = True
                    if random.randint(1, 100) < 40:
                        troop_two_mana = 8
                elif troop_one_mana >= troop_one_max_mana:
                    print('there is enough for another bee')
                    # cast goblin shaman here
                    second_troop_casts += 1
                    add_color_gems(board, 7, 2)
                    troop_one_mana = 0
                    extra_turn = True
                else:
                    loop_ends_no_mana += 1
                red_mana = 0
                green_mana = 0
                blue_mana = 0
                yellow_mana = 0
                brown_mana = 0
                purple_mana = 0

            else:
                loop_ends_no_extra += 1
                print("not enough for an extra turn")
                print("it went " + str(iterations) + " number of times")
                results.append(iterations)
                second_troop_results.append(second_troop_casts)
                iterations = 0
                second_troop_casts = 0

                enemy_turn(board, "destroy all")
                if len(results) > 1000:
                    print("bee casts")
                    print(statistics.mean(results))
                    print("shaman casts")
                    print(statistics.mean(second_troop_results))
                    print(results)
                    print("number of no mana")
                    print(loop_ends_no_mana)
                    print("number of no extra")
                    print(loop_ends_no_extra)
                    break


def enemy_turn(board, enemy_type):
    mana_board = create_mana_board()
    if enemy_type == "new board":
        for row in range(BOARD_SIZE):
            for col in range(BOARD_SIZE):
                board[row][col] = random.randint(1, NUM_GEM_TYPES)
    if enemy_type == "destroy all":
        for row in range(BOARD_SIZE):
            for col in range(BOARD_SIZE):
                board[row][col] = 0
        shift_down(board)
        fill_blanks(board)
    if enemy_type == "match 3":
        num_gems_removed = 0
        while num_gems_removed < 3:
            row = random.randint(0, BOARD_SIZE - 1)
            col = random.randint(0, BOARD_SIZE - 1)
            if board[row][col] != 0:
                board[row][col] = 0
                num_gems_removed += 1
        shift_down(board)
        fill_blanks(board)
    if enemy_type == "do nothing":
        # this does nothing
        shift_down(board)
        fill_blanks(board)
    enemy_turn_check_matches(board, mana_board)


def enemy_turn_check_matches(board, mana_board):
    # we check for matches on enemy turn and clear any matches before giving board back
    global red_mana
    global green_mana
    global blue_mana
    global yellow_mana
    global brown_mana
    global purple_mana
    global extra_turn

    while True:
        matches = find_matches(board, mana_board)
        if matches:
            remove_gems(board, matches, mana_board)
            fill_blanks(board)

        if len(matches) <= 0:
            red_mana = 0
            green_mana = 0
            blue_mana = 0
            yellow_mana = 0
            brown_mana = 0
            purple_mana = 0
            extra_turn = False
            break


def board_vision(board_image):
    redgem_image = cv2.imread('redgem.bmp', cv2.IMREAD_UNCHANGED)
    greengem_image = cv2.imread('greengem.bmp', cv2.IMREAD_UNCHANGED)
    bluegem_image = cv2.imread('bluegem.bmp', cv2.IMREAD_UNCHANGED)
    yellowgem_image = cv2.imread('yellowgem.bmp', cv2.IMREAD_UNCHANGED)
    browngem_image = cv2.imread('browngem.bmp', cv2.IMREAD_UNCHANGED)
    purplegem_image = cv2.imread('purplegem.bmp', cv2.IMREAD_UNCHANGED)
    skullgem_image = cv2.imread('skullgem.bmp', cv2.IMREAD_UNCHANGED)
    #board_image = cv2.imread('entireboard.png', cv2.IMREAD_UNCHANGED)
    cropped_board_image = board_image[80:1031, 480:1436]
    numberofgemsread=0
    board = [[8 for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)]
    for col in range(0,8):
        for row in range(0,8):
            topleftcornerx = 480 + (col) * 120
            topleftcornery = 80 + (row) * 120
            bottomrightcornerx = 480 + (col + 1) * 120
            bottomrightcornery = 80 + (row + 1) * 120


            temporary_tile = board_image[topleftcornery:bottomrightcornery, topleftcornerx:bottomrightcornerx]
            redgemresult = cv2.matchTemplate(temporary_tile, redgem_image, cv2.TM_CCOEFF_NORMED)
            greengemresult = cv2.matchTemplate(temporary_tile, greengem_image, cv2.TM_CCOEFF_NORMED)
            bluegemresult = cv2.matchTemplate(temporary_tile, bluegem_image, cv2.TM_CCOEFF_NORMED)
            yellowgemresult = cv2.matchTemplate(temporary_tile, yellowgem_image, cv2.TM_CCOEFF_NORMED)
            browngemresult = cv2.matchTemplate(temporary_tile, browngem_image, cv2.TM_CCOEFF_NORMED)
            purplegemresult = cv2.matchTemplate(temporary_tile, purplegem_image, cv2.TM_CCOEFF_NORMED)
            skullgemresult = cv2.matchTemplate(temporary_tile, skullgem_image, cv2.TM_CCOEFF_NORMED)

            min_val, max_val_red, min_loc, max_loc = cv2.minMaxLoc(redgemresult)
            min_val, max_val_green, min_loc, max_loc = cv2.minMaxLoc(greengemresult)
            min_val, max_val_blue, min_loc, max_loc = cv2.minMaxLoc(bluegemresult)
            min_val, max_val_yellow, min_loc, max_loc = cv2.minMaxLoc(yellowgemresult)
            min_val, max_val_brown, min_loc, max_loc = cv2.minMaxLoc(browngemresult)
            min_val, max_val_purple, min_loc, max_loc = cv2.minMaxLoc(purplegemresult)
            min_val, max_val_skull, min_loc, max_loc = cv2.minMaxLoc(skullgemresult)
            if max_val_red>0.8:
                print(max_val_red)
                print("max red")
                numberofgemsread+=1
                board[row][col]=1

            if max_val_green>0.8:
                print(max_val_green)
                print("max green")
                numberofgemsread+=1
                board[row][col]=2

            if max_val_blue>0.8:
                print(max_val_blue)
                print("max blue")
                numberofgemsread+=1
                board[row][col]=3

            if max_val_yellow>0.8:
                print(max_val_yellow)
                print("max yellow")
                numberofgemsread+=1
                board[row][col]=4

            if max_val_brown>0.8:
                print(max_val_brown)
                print("max brown")
                numberofgemsread+=1
                board[row][col]=5


            if max_val_purple>0.8:
                print(max_val_purple)
                print("max purple")
                numberofgemsread+=1
                board[row][col]=6


            if max_val_skull>0.8:
                print(max_val_skull)
                print("max skull")
                numberofgemsread+=1
                board[row][col]=7

    print(numberofgemsread)
    print(board)
    return(board)

def take_screenshot():
    #something here
    image = pyautogui.screenshot()
    image = cv2.cvtColor(np.array(image),
                         cv2.COLOR_RGB2BGR)
    board=board_vision(image)
    check_four_matches(board)
def toggle_team_select():
    if teamselectframe.hidden==1:
        teamselectframe.hidden=0
        teamselectframe.pack(side=tkinter.RIGHT)
    else:
        teamselectframe.hidden=1
        teamselectframe.pack_forget()
def toggle_extra_turn_info():
    if infoframe.hidden==1:
        infoframe.hidden=0
        infoframe.pack(side=tkinter.RIGHT)
    else:
        infoframe.hidden=1
        infoframe.pack_forget()
def UI_setup():
    B = tkinter.Button(buttonframe, text="Take Screencapture", command = lambda: [take_screenshot()])
    B.pack(side=tkinter.TOP)
    pickteambutton = tkinter.Button(buttonframe, text="Pick team", command= lambda: [toggle_team_select()])
    pickteambutton.pack(side=tkinter.TOP)
    showextrabutton = tkinter.Button(buttonframe, text="Show extra turns", command= lambda: [toggle_extra_turn_info()])
    showextrabutton.pack(side=tkinter.TOP)
    infolabel = tkinter.Label(infoframe, textvariable=infoTextVar)
    infolabel.pack(side=tkinter.BOTTOM)

    tkinter.Checkbutton(teamselectframe, text='Divine Ishbaala', variable=divineishbaala, onvalue=True, offvalue=False).grid(row=0, column=0)
    tkinter.Checkbutton(teamselectframe, text='Draxxius', variable=draxxius, onvalue=True, offvalue=False).grid(row=0, column=1)
    tkinter.Checkbutton(teamselectframe, text='Tai-Pan', variable=taipan, onvalue=True, offvalue=False).grid(row=0, column=2)
    tkinter.Checkbutton(teamselectframe, text='Eldritch Minion', variable=eldritchminion, onvalue=True, offvalue=False).grid(row=1, column=0)
    tkinter.Checkbutton(teamselectframe, text='Bane of Mercy', variable=baneofmercy, onvalue=True, offvalue=False).grid(row=1, column=1)
    tkinter.Checkbutton(teamselectframe, text='Mercy', variable=mercy, onvalue=True, offvalue=False).grid(row=1, column=2)
    tkinter.Checkbutton(teamselectframe, text='Veneratus', variable=veneratus, onvalue=True, offvalue=False).grid(row=2, column=0)
    tkinter.Checkbutton(teamselectframe, text='Malcandessa', variable=malcandessa, onvalue=True, offvalue=False).grid(row=2, column=1)
    tkinter.Checkbutton(teamselectframe, text='Moonsinger', variable=moonsinger, onvalue=True, offvalue=False).grid(row=2, column=2)
    tkinter.Checkbutton(teamselectframe, text='Astral Spirit', variable=astralspirit, onvalue=True, offvalue=False).grid(row=3, column=0)
    tkinter.Checkbutton(teamselectframe, text='Apophisis', variable=apophisis, onvalue=True, offvalue=False).grid(row=3, column=1)
    tkinter.Checkbutton(teamselectframe, text='Trknala', variable=trknala, onvalue=True, offvalue=False).grid(row=3, column=2)
    tkinter.Checkbutton(teamselectframe, text='Gimlet Stormbrew', variable=gimletstormbrew, onvalue=True, offvalue=False).grid(row=4, column=0)
    tkinter.Checkbutton(teamselectframe, text='Grave Seer', variable=graveseer, onvalue=True, offvalue=False).grid(row=4, column=1)
    tkinter.Checkbutton(teamselectframe, text='Lamashtu', variable=lamashtu, onvalue=True, offvalue=False).grid(row=4, column=2)
    tkinter.Checkbutton(teamselectframe, text='Scurvy Seadog', variable=scurvyseadog, onvalue=True, offvalue=False).grid(row=5, column=0)
    tkinter.Checkbutton(teamselectframe, text='Inari', variable=inari, onvalue=True, offvalue=False).grid(row=5, column=1)
    tkinter.Checkbutton(teamselectframe, text='Child of Summer', variable=childofsummer, onvalue=True, offvalue=False).grid(row=5, column=2)
    tkinter.Checkbutton(teamselectframe, text='First Mate Axelubber', variable=firstmateaxelubber, onvalue=True, offvalue=False).grid(row=6, column=0)
    tkinter.Checkbutton(teamselectframe, text='Moon Rabbit', variable=moonrabbit, onvalue=True, offvalue=False).grid(row=6, column=1)
    tkinter.Checkbutton(teamselectframe, text='Spider Throne', variable=spiderthrone, onvalue=True, offvalue=False).grid(row=6, column=2)
    tkinter.Checkbutton(teamselectframe, text='Fist of Zorn', variable=fistofzorn, onvalue=True, offvalue=False).grid(row=7, column=0)
    tkinter.Checkbutton(teamselectframe, text='Drowned Sailor', variable=drownedsailor, onvalue=True, offvalue=False).grid(row=7, column=1)
    tkinter.Checkbutton(teamselectframe, text='Daughter of Ice', variable=daughterofice, onvalue=True, offvalue=False).grid(row=7, column=2)
    tkinter.Checkbutton(teamselectframe, text='Herald of damnation', variable=heraldofdamnation, onvalue=True, offvalue=False).grid(row=8, column=0)
    tkinter.Checkbutton(teamselectframe, text='Spirit Fox', variable=spiritfox, onvalue=True, offvalue=False).grid(row=8, column=1)
    tkinter.Checkbutton(teamselectframe, text='Yao Guai', variable=yaoguai, onvalue=True, offvalue=False).grid(row=8, column=2)
    tkinter.Checkbutton(teamselectframe, text='Doomed Axe', variable=doomedaxe, onvalue=True, offvalue=False).grid(row=9, column=0)
    tkinter.Checkbutton(teamselectframe, text='Doomed Club', variable=doomedclub, onvalue=True, offvalue=False).grid(row=9, column=1)
    tkinter.Checkbutton(teamselectframe, text='Doomed Blade', variable=doomedblade, onvalue=True, offvalue=False).grid(row=9, column=2)
    tkinter.Checkbutton(teamselectframe, text='Doomed Glaive', variable=doomedglaive, onvalue=True, offvalue=False).grid(row=10, column=0)
    tkinter.Checkbutton(teamselectframe, text='Doomed Scythe', variable=doomedscythe, onvalue=True, offvalue=False).grid(row=10, column=1)
    tkinter.Checkbutton(teamselectframe, text='Doomed Crossbow', variable=doomedcrossbow, onvalue=True, offvalue=False).grid(row=10, column=2)
    tkinter.Checkbutton(teamselectframe, text='Alderfather', variable=alderfather, onvalue=True, offvalue=False).grid(row=11, column=0)
    tkinter.Checkbutton(teamselectframe, text='Nimue', variable=nimue, onvalue=True, offvalue=False).grid(row=11, column=1)
    tkinter.Checkbutton(teamselectframe, text='Forest Guardian', variable=forestguardian, onvalue=True, offvalue=False).grid(row=11, column=2)
    tkinter.Checkbutton(teamselectframe, text='Qilin', variable=qilin, onvalue=True, offvalue=False).grid(row=12, column=0)
    tkinter.Checkbutton(teamselectframe, text='Sekhma', variable=sekhma, onvalue=True, offvalue=False).grid(row=12, column=1)
    tkinter.Checkbutton(teamselectframe, text='Beast Master Torbern', variable=beastmastertorbern, onvalue=True, offvalue=False).grid(row=12, column=2)
    tkinter.Checkbutton(teamselectframe, text='Infernal King', variable=infernalking, onvalue=True, offvalue=False).grid(row=13, column=0)
    tkinter.Checkbutton(teamselectframe, text='Sir Quentin Hadley', variable=sirquentinhadley, onvalue=True, offvalue=False).grid(row=13, column=1)
    tkinter.Checkbutton(teamselectframe, text='Wrath', variable=wrath, onvalue=True, offvalue=False).grid(row=13, column=2)
    tkinter.Checkbutton(teamselectframe, text='Glaycion', variable=glaycion, onvalue=True, offvalue=False).grid(row=14, column=0)





# Start the game
board = create_board()
UI_setup()
take_screenshot()
#board = [[7, 7, 5, 7, 7, 4, 7, 1], [2, 3, 6, 7, 4, 7, 7, 6], [5, 6, 6, 5, 2, 4, 5, 7], [4, 1, 5, 1, 7, 7, 4, 5],
#         [3, 3, 3, 4, 7, 3, 6, 4], [2, 3, 2, 4, 2, 3, 3, 5], [7, 7, 1, 3, 4, 6, 7, 3], [6, 5, 6, 5, 7, 1, 7, 2]]
#board_image = cv2.imread('entireboard.png', cv2.IMREAD_UNCHANGED)
#board = board_vision(board_image)
#check_four_matches(board)
# play_game()
